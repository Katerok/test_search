#include <PageParserManager.h>
#include <PageParser.h>
#include <QWidget>
#include <QThread>

PageParserManager::PageParserManager(QLayout *aResLayout, QObject *parent)
    : QObject(parent)
    , m_res_layout(aResLayout)
    , m_threads_num_max(0)
{
}

PageParserManager::~PageParserManager()
{
    deleteAllOnStop();
}

void PageParserManager::start(const QString& aUrl,
                              const QString& aSearchFor,
                              unsigned int aScanUrlNum,
                              unsigned int aScanThreadsNum)
{
    clearAll();

    m_urls_set.insert(aUrl);
    m_scan_url_num = aScanUrlNum - 1;
    m_threads_num_max = aScanThreadsNum;
    m_search_for = aSearchFor;
    m_urls_processed = 0;

    for (unsigned int i = 0; i < m_threads_num_max; ++i)
    {
        QThread * thread = new QThread();
        thread->start();
        m_threads_queue.enqueue(thread);
    }

    PageParser *root_parser = new PageParser(aUrl, 0, m_search_for);
    m_res_layout->addWidget(root_parser->getUrlItemWidget());
    runPageParser(root_parser);
}

void PageParserManager::deleteAllOnStop()
{
    while (!m_threads_queue.isEmpty())
    {
        QThread * thread = m_threads_queue.dequeue();
        thread->exit(0);
        thread->deleteLater();
    }

    QSet<PageParser *>::iterator i;
    for (i = m_currently_running.begin(); i != m_currently_running.end(); ++i)
        delete *i;
    m_currently_running.clear();

    while (!m_current_queue.isEmpty())
        delete m_current_queue.dequeue();
}

void PageParserManager::clearAll()
{
    deleteAllOnStop();

    // Delete all URLItems
    QLayoutItem* item;
    while ((item = m_res_layout->layout()->takeAt(0)) != NULL)
    {
        delete item->widget();
        delete item;
    }

    m_urls_set.clear();
    m_search_for.clear();
    m_current_queue.clear();
    m_is_paused = false;
}

void PageParserManager::updateParsersQueue(PageParser* aPageParser)
{
    if (!m_scan_url_num)
        return;

    unsigned int next_level = aPageParser->getLevel() + 1;
    const QSet<QString>& urls = aPageParser->getUrlSet();

    QSet<QString>::const_iterator i;
    for (i = urls.begin(); i != urls.end(); ++i)
    {
        if (!m_scan_url_num)
            return;

        if (!m_urls_set.contains(*i))
        {
            --m_scan_url_num;
            m_urls_set.insert(*i);
            PageParser* m_new_parser = new PageParser(*i,
                                                      next_level,
                                                      m_search_for);
            m_current_queue.enqueue(m_new_parser);
            m_res_layout->addWidget(m_new_parser->getUrlItemWidget());
        }
    }
}

void PageParserManager::onLoadDone(bool aIsSucessfull,
                                   PageParser* aPageParser)
{
    emit progressUpdate(++m_urls_processed);
    m_threads_queue.enqueue(aPageParser->thread());

    if (m_is_paused)
        return;

    if (aIsSucessfull)
        updateParsersQueue(aPageParser);

    m_currently_running.remove(aPageParser);
    aPageParser->deleteLater();

    while (!m_threads_queue.isEmpty() && !m_current_queue.isEmpty())
        runPageParser(m_current_queue.dequeue());

    if (static_cast<unsigned int>(m_threads_queue.count()) == m_threads_num_max)
        emit pagesScanDone();
}

void PageParserManager::runPageParser(PageParser *aPageParser)
{
    connect(aPageParser, SIGNAL(urlProcessingDone(bool, PageParser*)),
            this, SLOT(onLoadDone(bool, PageParser*)), Qt::QueuedConnection);

    QThread *thread = m_threads_queue.dequeue();
    aPageParser->moveToThread(thread);

    aPageParser->run(&m_network_manager);
    m_currently_running.insert(aPageParser);
}

void PageParserManager::stop()
{
    m_is_paused = true;
    QSet<PageParser *>::const_iterator i;
    for (i = m_currently_running.begin(); i != m_currently_running.end(); ++i)
        (*i)->stop();
}

void PageParserManager::resume()
{
    m_is_paused = false;
    QSet<PageParser *>::const_iterator i;
    for (i = m_currently_running.begin(); i != m_currently_running.end(); ++i)
        (*i)->run(&m_network_manager);
}

