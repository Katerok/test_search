#include <UrlItemWidget.h>
#include <PageParser.h>
#include <FileLoader.h>

#include <QTextDocumentFragment>
#include <QMetaType>

PageParser::PageParser(QUrl aUrl, unsigned int aLevel, const QString &aSearchFor)
    : m_is_downloaded(false)
    , m_file_loader(NULL)
    , m_level(aLevel)
    , m_url(aUrl)
    , m_search_for(aSearchFor)
{
    m_uml_item = new UrlItemWidget(aLevel, aUrl);

    qRegisterMetaType<UrlItemState>("UrlItemState");
    connect(this, SIGNAL(setState(UrlItemState)),
            m_uml_item, SLOT(onSetState(UrlItemState)), Qt::QueuedConnection);

    connect(this, SIGNAL(setupErrorDialog(QString)),
            m_uml_item, SLOT(onSetupErrorDialog(QString)),
            Qt::QueuedConnection);

    qRegisterMetaType<QSet<QString> >("QSet<QString>");
    connect(this, SIGNAL(setupDataDialog(QString,unsigned int, const QSet<QString>&)),
            m_uml_item, SLOT(onSetupDataDialog(QString, unsigned int, const QSet<QString>&)),
            Qt::QueuedConnection);
}

PageParser::~PageParser()
{
    if (m_file_loader)
        delete m_file_loader;
}

void PageParser::onError(const QString &aErrorMessage)
{
    emit setState(ERROR);
    emit setupErrorDialog(aErrorMessage);
    emit urlProcessingDone(false, this);
}

void PageParser::run(QNetworkAccessManager *aManager)
{
    m_file_loader = new FileLoader(m_url, aManager);
    emit setState(DOWNLOADING);

    connect(m_file_loader, SIGNAL(loadDone(QString)),
            this, SLOT(onLoadDone(QString)));
}

void PageParser::onLoadDone(QString aErrString)
{
    m_is_downloaded = true;

    if (aErrString.isEmpty())
        scanPage();
    else
        onError(aErrString);
}

void PageParser::scanPage()
{
    emit setState(SCANNING);

    searchForUrls();

    unsigned int entry_count = searchForStr(m_search_for);

    if (entry_count)
        emit setState(FOUND);
    else
        emit setState(NOT_FOUND);

    emit setupDataDialog(m_search_for, entry_count, m_unique_urls);
    emit urlProcessingDone(true, this);
}

void PageParser::searchForUrls()
{
    QTextDocument text_document(m_file_loader->getData().constData());

    QRegExp regexp_start("href[ ]*=[ ]*[\\\"\\\']", Qt::CaseInsensitive);
    QRegExp regexp_url("http[s]?://[0-9a-z\\.\\-/:_%+]+", Qt::CaseInsensitive);

    QTextCursor current_cursor = text_document.find(regexp_start);
    QTextCursor url_cursor = text_document.find(regexp_start);
    while (!current_cursor.isNull())
    {
        url_cursor = text_document.find(regexp_url, current_cursor);

        if (url_cursor.isNull())
            current_cursor = text_document.find(regexp_start, current_cursor);
        else
        {
            if (url_cursor.selectionStart() == current_cursor.selectionEnd())
                m_unique_urls.insert(url_cursor.selection().toPlainText());
            current_cursor = text_document.find(regexp_start, url_cursor);
        }
    }
}

unsigned int PageParser::searchForStr(const QString &aSearchFor) const
{
    QTextDocument text_document;
    text_document.setHtml(m_file_loader->getData().constData());
    return text_document.toPlainText().count(aSearchFor);
}

const QSet<QString>& PageParser::getUrlSet() const
{
    return m_unique_urls;
}

unsigned int PageParser::getLevel() const
{
    return m_level;
}

QWidget * PageParser::getUrlItemWidget() const
{
    return m_uml_item;
}

void PageParser::stop()
{
    if (!m_is_downloaded && m_file_loader)
    {
        emit setState(ABORTED);
        m_file_loader->stop();
        delete m_file_loader;
        m_file_loader = NULL;
    }
}

