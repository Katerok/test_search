#include <FileLoader.h>
#include <QNetworkReply>

FileLoader::FileLoader(QUrl aUrl, QNetworkAccessManager *aManager)
{
    m_reply = aManager->get(QNetworkRequest(aUrl));

    connect(m_reply, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(m_reply, SIGNAL(finished()), this, SLOT(onFinished()));
}

FileLoader::~FileLoader()
{
}

void FileLoader::onReadyRead()
{
    QVariant attr =
        m_reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
    if (attr.isValid())
    {
        QString err_string;
        err_string.append("Error: redirection is not supported. \n"
                          "Redirected to: ");
        err_string.append(attr.toString());
        emit loadDone(err_string);
        stop();
        return;
    }

    QVariant type_header = m_reply->header(QNetworkRequest::ContentTypeHeader);
    if (!type_header.toString().contains("text/html"))
    {
        QString err_string;
        err_string.append("Error: content type \"");
        err_string.append(type_header.toString());
        err_string.append("\" is not supported");

        emit loadDone(err_string);
        stop();
    }
}

void FileLoader::onFinished()
{
    QVariant attr = m_reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    QString err_string;
    if (attr.toInt() != 200)
    {
        err_string.append("Error #");
        err_string.append(QString::number(attr.toInt()));
        err_string.append(": ");
        err_string.append(m_reply->errorString());
    }
    else
        m_data = m_reply->readAll();

    m_reply->deleteLater();
    m_reply = NULL;

    emit loadDone(err_string);
}

void FileLoader::stop()
{
    if (m_reply)
    {
        disconnect(m_reply, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
        disconnect(m_reply, SIGNAL(finished()), this, SLOT(onFinished()));
        m_reply->abort();
        m_reply->deleteLater();
        m_reply = NULL;
    }
}

const QByteArray& FileLoader::getData() const
{
    return m_data;
}

