#include <MainWindow.h>
#include <QMessageBox>
#include <QThread>
#include <QUrl>

#include <PageParserManager.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

    ui.vl_scroll_area_content->setAlignment(Qt::AlignTop);
    ui.sa_results->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    m_parser_manager = new PageParserManager(ui.vl_scroll_area_content, this);
    ui.sb_threads_num->setValue(QThread::idealThreadCount());

    m_progress_bar.setMaximumHeight(16);
    m_progress_bar.setMaximumWidth(200);
    m_progress_bar.setTextVisible(false);
    this->statusBar()->addPermanentWidget(&m_progress_bar, 0);

    connect(ui.pb_start, SIGNAL(clicked()), this, SLOT(onStart()));
    connect(ui.pb_stop, SIGNAL(clicked()), this, SLOT(onStop()));
    connect(ui.pb_pause, SIGNAL(clicked()), this, SLOT(onPause()));
    connect(ui.pb_resume, SIGNAL(clicked()), this, SLOT(onResume()));
    connect(m_parser_manager, SIGNAL(pagesScanDone()),
            this, SLOT(onPagesScanDone()), Qt::QueuedConnection);
    connect(m_parser_manager, SIGNAL(progressUpdate(int)),
            this, SLOT(onProgressUpdate(int)), Qt::QueuedConnection);
}

MainWindow::~MainWindow()
{
    delete m_parser_manager;
}

void MainWindow::setupInterfaceEditable(bool aIsEditable)
{
    ui.le_url->setReadOnly(aIsEditable);
    ui.le_search_for->setReadOnly(aIsEditable);
    ui.sb_scan_url_num->setReadOnly(aIsEditable);
    ui.pb_start->setEnabled(!aIsEditable);
    ui.pb_stop->setEnabled(aIsEditable);
    ui.pb_pause->setEnabled(aIsEditable);
    ui.pb_resume->setEnabled(false);
}

void MainWindow::onStart()
{
    if (ui.le_url->text().isEmpty() ||
        ui.le_search_for->text().isEmpty())
    {
        QMessageBox::critical(this, tr("Error"),
                              tr("Please, enter URL and Search Request."));

        return;
    }

    m_parser_manager->start(ui.le_url->text(),
                            ui.le_search_for->text(),
                            ui.sb_scan_url_num->value(),
                            ui.sb_threads_num->value());

    setupInterfaceEditable(true);
    this->statusBar()->showMessage(QString("Processing..."));
    m_progress_bar.setMaximum(ui.sb_scan_url_num->value());
}

void MainWindow::onStop()
{
    m_parser_manager->stop();
    setupInterfaceEditable(false);
    this->statusBar()->showMessage(QString("Stopped"));
}

void MainWindow::onPause()
{
    m_parser_manager->stop();
    ui.pb_resume->setEnabled(true);
    ui.pb_pause->setEnabled(false);
    this->statusBar()->showMessage(QString("Paused"));
}

void MainWindow::onResume()
{
    m_parser_manager->resume();
    ui.pb_resume->setEnabled(false);
    ui.pb_pause->setEnabled(true);
    this->statusBar()->showMessage(QString("Processing..."));
}

void MainWindow::onPagesScanDone()
{
    setupInterfaceEditable(false);
    this->statusBar()->showMessage(QString("Done"));
}

void MainWindow::onProgressUpdate(int aUrlsProcessed)
{
    m_progress_bar.setValue(aUrlsProcessed);
}

