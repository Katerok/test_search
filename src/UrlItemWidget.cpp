#include <UrlItemWidget.h>
#include <QDesktopServices>

UrlItemWidget::UrlItemWidget(unsigned int aLevel, const QUrl& aUrl)
    : QWidget(NULL)
    , m_url(aUrl)
{
    ui.setupUi(this);

    ui.lb_url->setText(m_url.url());
    ui.lb_level->setText(QString::number(aLevel));
}

UrlItemWidget::~UrlItemWidget()
{
}

void UrlItemWidget::onSetState(UrlItemState aState)
{
    switch (aState)
    {
     case (WAITING):
        ui.lb_state->setText("Waiting..");
        this->setStyleSheet("QWidget {background-color : rgb(240, 240, 240);}");
        break;
     case (DOWNLOADING):
        ui.lb_state->setText("Downloading..");
        this->setStyleSheet("QWidget {background-color : rgb(220, 220, 255);}");
        break;
     case (SCANNING):
        ui.lb_state->setText("Scanning..");
        this->setStyleSheet("QWidget {background-color : rgb(200, 200, 255);}");
        break;
     case (ERROR):
        ui.lb_state->setText("Error");
        this->setStyleSheet("QWidget {background-color : rgb(255, 200, 200);}");
        break;
     case (ABORTED):
        ui.lb_state->setText("Aborted..");
        this->setStyleSheet("QWidget {background-color : rgb(255, 220, 220);}");
        break;
     case (FOUND):
        ui.lb_state->setText("Found");
        this->setStyleSheet("QWidget {background-color : rgb(200, 255, 200);}");
        break;
     case (NOT_FOUND):
        ui.lb_state->setText("Not Found");
        this->setStyleSheet("QWidget {background-color : rgb(220, 255, 220);}");
        break;
    }
}

void UrlItemWidget::showDialog()
{
    m_dialog.show();
}

void UrlItemWidget::onGotoPressed()
{
    QDesktopServices::openUrl(m_url);
}

void UrlItemWidget::onSetupDataDialog(QString aSearchFor,
                                      unsigned int aEntryCount,
                                      const QSet<QString>& aUrlSet)
{
    ui_details_dialog.setupUi(&m_dialog);

    QSet<QString>::const_iterator i;
    for (i = aUrlSet.begin(); i != aUrlSet.end(); ++i)
        ui_details_dialog.lw_found_urls->addItem(*i);

    ui_details_dialog.le_entry_num->setText(QString::number(aEntryCount));

    ui_details_dialog.le_url->setText(m_url.url());
    ui_details_dialog.le_search_for->setText(aSearchFor);

    connect(ui.pb_get_details, SIGNAL(clicked()), this, SLOT(showDialog()));
    connect(ui_details_dialog.pb_goto, SIGNAL(clicked()),
            this, SLOT(onGotoPressed()));

    ui.pb_get_details->setEnabled(true);
    m_dialog.setParent(dynamic_cast<QWidget *>(this->parent()), Qt::Dialog);
}

void UrlItemWidget::onSetupErrorDialog(QString aErrorMessage)
{
    ui_error_dialog.setupUi(&m_dialog);

    ui_error_dialog.le_url->setText(m_url.url());
    ui_error_dialog.te_error_message->setText(aErrorMessage);

    connect(ui.pb_get_details, SIGNAL(clicked()), this, SLOT(showDialog()));
    connect(ui_error_dialog.pb_goto, SIGNAL(clicked()),
            this, SLOT(onGotoPressed()));

    ui.pb_get_details->setEnabled(true);
    ui.pb_get_details->setText("Get Error");

    m_dialog.setParent(dynamic_cast<QWidget *>(this->parent()), Qt::Dialog);
}

