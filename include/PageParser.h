#ifndef PAGEPARSER_H
#define PAGEPARSER_H

#include <QSet>
#include <QUrl>
#include <QNetworkAccessManager>
#include <UrlItemState.h>

class FileLoader;
class UrlItemWidget;

class PageParser : public QObject
{
Q_OBJECT

public:
    PageParser(QUrl aUrl, unsigned int aLevel, const QString &aSearchFor);
    virtual ~PageParser();

    QWidget* getUrlItemWidget() const;
    const QSet<QString>& getUrlSet() const;
    unsigned int getLevel() const;

    void run(QNetworkAccessManager *aManager);
    void resume(QNetworkAccessManager *aManager);
    void stop();

signals:
    void urlProcessingDone(bool aIsSucessfull, PageParser* aPageParser);
    void setState(UrlItemState aState);
    void setupErrorDialog(QString aErrorMessage);
    void setupDataDialog(QString aSerchFor, unsigned int aEntryCount, const QSet<QString>& aUrlSet);

private slots:
    void onLoadDone(QString aErrString);

private:
    void scanPage();
    void searchForUrls();
    unsigned int searchForStr(const QString &aSearchFor) const;
    void onError(const QString &aErrorMessage);

    bool m_is_downloaded;

    FileLoader      *m_file_loader;
    UrlItemWidget   *m_uml_item;
    unsigned int    m_level;

    QUrl            m_url;
    QSet<QString>   m_unique_urls;
    const QString   &m_search_for;
};
 
#endif // PAGEPARSER_H
