#ifndef FILELOADER_H
#define FILELOADER_H

#include <QNetworkAccessManager>

class FileLoader : public QObject
{
    Q_OBJECT

public:
    FileLoader(QUrl imageUrl, QNetworkAccessManager *aManager);
    virtual ~FileLoader();

    const QByteArray& getData() const;
    void stop();

signals:
    void loadDone(QString);

private slots:
    void onFinished();
    void onReadyRead();

private:
    QByteArray m_data;
    QNetworkReply* m_reply;
};

#endif // FILELOADER_H
