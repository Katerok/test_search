#ifndef PAGEPARSERMANAGER_H
#define PAGEPARSERMANAGER_H

#include <QLayout>
#include <QQueue>
#include <QNetworkAccessManager>

class QThread;
class PageParser;

class PageParserManager : public QObject
{
Q_OBJECT

public:
    PageParserManager(QLayout *aResLayout, QObject *parent);
    virtual ~PageParserManager();

    void start(const QString& aUrl, const QString& aSearchFor,
               unsigned int aScanUrlNum, unsigned int aScanThreadsNum);
    void stop();
    void resume();

signals:
    void pagesScanDone();
    void progressUpdate(int aUrlsProcessed);

private slots:
    void onLoadDone(bool aIsSucessfull, PageParser* aPageParser);

private:
    void runPageParser(PageParser *aPageParser);
    void updateParsersQueue(PageParser* aPageParser);
    void clearAll();
    void deleteAllOnStop();

    QLayout     *m_res_layout;

    QSet<QString>           m_urls_set;
    QQueue<PageParser *>    m_current_queue;
    QSet<PageParser *>      m_currently_running;
    QQueue<QThread *>       m_threads_queue;

    unsigned int            m_scan_url_num;
    unsigned int            m_threads_num_max;
    int                     m_urls_processed;
    QString                 m_search_for;
    bool                    m_is_paused;

    QNetworkAccessManager   m_network_manager;
};
 
#endif // PAGEPARSERMANAGER_H
