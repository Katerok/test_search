#ifndef URLITEMSTATE_H
#define URLITEMSTATE_H

typedef enum
{
    WAITING,
    DOWNLOADING,
    SCANNING,
    ERROR,
    ABORTED,
    FOUND,
    NOT_FOUND
} UrlItemState;

#endif // URLITEMSTATE_H
