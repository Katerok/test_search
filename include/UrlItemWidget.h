#ifndef URLITEMWIDGET_H
#define URLITEMWIDGET_H

#include "ui_UrlItem.h"
#include "ui_GetDetailsDialog.h"
#include "ui_GetErrorDialog.h"

#include <QUrl>
#include <UrlItemState.h>

class UrlItemWidget : public QWidget
{
Q_OBJECT

public:
    UrlItemWidget(unsigned int aLevel, const QUrl& aUrl);
    virtual ~UrlItemWidget();

private slots:
    void showDialog();
    void onGotoPressed();
    void onSetState(UrlItemState aStatse);

    void onSetupErrorDialog(QString aErrorMessage);
    void onSetupDataDialog(QString aSerchFor, unsigned int aEntryCount, const QSet<QString>& aUrlSet);

private:
    Ui::UrlItem ui;
    Ui::GetDetailsDialog ui_details_dialog;
    Ui::GetErrorDialog ui_error_dialog;

    QDialog m_dialog;
    QUrl m_url;
};

#endif // URLITEMWIDGET_H

