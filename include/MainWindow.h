#include "ui_MainWindow.h"
#include <QProgressBar>

class PageParserManager;

class MainWindow : public QMainWindow
{
Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void onStart();
    void onStop();
    void onPause();
    void onResume();
    void onPagesScanDone();
    void onProgressUpdate(int);

private:
    void setupInterfaceEditable(bool aIsEditable);

    Ui::MainWindow      ui;
    PageParserManager   *m_parser_manager;
    QProgressBar        m_progress_bar;
};
